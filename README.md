# OpenML dataset: rossi

https://www.openml.org/d/41438

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: [Peter Henry Rossi](https://en.wikipedia.org/wiki/Peter_H._Rossi) et al.
**Source**: [lifelines.datasets](https://github.com/CamDavidsonPilon/lifelines/blob/master/lifelines/datasets/rossi.csv) - 1980  
**Please cite**: Rossi, P.H., Berk, R.A., and Lenihan, K.J. (1980). Money, Work, and Crime: Some Experimental Results. New York: Academic.

The data pertain to 432 convicts who were released from Maryland state prisons in the 1970s and who were followed up for one year after release. Half the released convicts were assigned at random to an experimental treatment in which they were given financial aid; half did not receive aid.

week - week of first arrest after release or censoring
arrest - 1 if arrested, 0 if not arrested.
fin - received financial aid?
age - age in years at time of release.
race - is black
wexp - full-time work experience before incarceration: no or yes.
mar - marital status at time of release: married or not married.
paro - released on parole?
prio - number of convictions prior to current incarceration.
educ - level of education: 2 = 6th grade or less; 3 = 7th to 9th grade; 4 = 10th to 11th grade; 5 = 12th grade; 6 = some college.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41438) of an [OpenML dataset](https://www.openml.org/d/41438). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41438/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41438/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41438/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

